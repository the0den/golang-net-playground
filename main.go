package main

import (
	"fmt"
	"net"
)

func delegated(domain string, to map[string]struct{}) bool {
	nameserver, err := net.LookupNS(domain)
	if err != nil {
		panic(err)
	}

	for _, ns := range nameserver {
		if _, ok := to[ns.Host]; ok {
			return true
		}

		// resolving non-ip hosts
		ips, err := net.LookupIP(ns.Host)
		if err != nil {
			panic(err)
		}

		for _, ip := range ips {
			if _, ok := to[ip.String()]; ok {
				return true
			}
		}
	}
	return false
}

func main() {
	nsIPs := map[string]struct{}{
		"127.0.0.1":  {},
	}
	fmt.Println(delegated("google.com", nsIPs))
}
